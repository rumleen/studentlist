package sheridan.rumleen.student;

import java.util.Arrays;
import sheridan.rumleen.student.Student;
import java.util.Scanner;

/**
 * This class is a simple example of creating arrays of objects
 *
 * @author Paul Bonenfant
 */
public class StudentList {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Student[] students = new Student[2];
        
        for (int i = 0; i < students.length; i++) {
        
           
            Student student = new Student(args[0], args[1]);
            
            students[i] = student;       
        }
        
        
        
        String format = "The student's name is %s\n and their id is %s";
        
        for (Student student: students) {
        
            System.out.printf(format, student.getName(), student.getId());
            
        }
        
    }

}

