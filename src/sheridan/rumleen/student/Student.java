package sheridan.rumleen.student;

/**
 * This class represents students in our application
 *
 * @author Paul Bonenfant
 */
public class Student {
    
    private String name;
    private String id;
    private String Program;

    /**
     * Get the value of Program
     *
     * @return the value of Program
     */
    public String getProgram() {
        return Program;
    }

    /**
     * Set the value of Program
     *
     * @param Program new value of Program
     */
    public void setProgram(String Program) {
        this.Program = Program;
    }

    //private static int numberOfStudents = 0;
    

    public Student(String name, String id) {
        this.name = name;
        this.id = id; //String.valueOf(numberOfStudents); 
        //numberOfStudents++;
    }

    public String getName() {
        return name;
    }
    
    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    

}

