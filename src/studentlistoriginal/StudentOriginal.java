package studentlistoriginal;

/**
 * This class represents students in our application
 *
 * @author Paul Bonenfant
 */
public class StudentOriginal {
    
    private String name;

    public StudentOriginal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    

}

